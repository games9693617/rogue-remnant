extends CharacterBody2D

@export var max_speed: float


func _ready():
	pass


func _process(_delta):
	velocity = get_movement_vector().normalized() * max_speed; 
	move_and_slide();


func get_movement_vector():
	return Vector2(
		# X Axis
		Input.get_action_strength("move_right") - \
		Input.get_action_strength("move_left"), \
		
		# Y Axis
		Input.get_action_strength("move_down") - \
		Input.get_action_strength("move_up")
	);
