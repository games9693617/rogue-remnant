extends Node

# Maximum distance the player can attack enemies from (in pixels)
@export var max_range: int
# PackedScene reference to the sword attack animation
@export var sword_ability: PackedScene;


func _ready():
	# Connect the "timeout" signal from the Timer to the on_timer_timeout function
	$Timer.timeout.connect(on_timer_timeout);

# Attempts to find the player node in the scene tree based on the "player" 
# group name. Returns the player Node2D if found, otherwise returns null.
func get_player_node_from_scene_tree():
	var player = get_tree().get_first_node_in_group("player") as Node2D;

	if player == null:
		return null;

	return player;

# Retrieves all nodes in the scene tree that belong to the "enemy" group.
# Casts the retrieved nodes to an Array of Node2D Returns an empty array if 
# no enemies are found, otherwise returns the array of enemies nodes.
func get_list_of_basic_enemies_from_scene_tree():
	var enemies = get_tree().get_nodes_in_group("enemy") as Array[Node2D];

	if enemies.is_empty():
		return []
	
	return enemies as Array[Node2D];

# Calculates the squared distance between the player and the enemy.
# Compares the squared distance to the square of the max_range.
# Returns true if the enemy is within range, false otherwise.
func is_enemy_in_range(enemy: Node2D, player: Node2D):
	var distance_between_player_and_enemy = \
		enemy.global_position.distance_squared_to(player.global_position)
	
	return distance_between_player_and_enemy < pow(max_range, 2);

# This function is called periodically by the Timer node, the timer node
# emits a signal when the count go to zero.
# It retrieves the player and enemy nodes, filters enemies in range,
# and if there's an enemy in range, positions and rotates the sword 
# ability animation.
func on_timer_timeout():
	# Get enemies and the player node
	var player = get_player_node_from_scene_tree() as Node2D;
	var enemies = get_list_of_basic_enemies_from_scene_tree() as Array[Node2D];

	# Filter enemies based on range (keep only enemies within max_range of the player)
	enemies = enemies.filter(func(enemy: Node2D) -> bool:
		return is_enemy_in_range(enemy, player);
	);

	# Filter both enemies node a and b to know which is
	# closeest to the player (because will be attacked first)
	enemies.sort_custom(
		func(a: Node2D, b: Node2D):
			var a_distance = a.global_position.distance_squared_to(
				player.global_position
			) as float;
			
			var b_distance = b.global_position.distance_squared_to(
				player.global_position
			) as float;
			
			return a_distance < b_distance;
	);

	# Instantiate the sword ability animation
	var sword_instance = sword_ability.instantiate() as Node2D;

	# Execute the attack by adding the animation to
	# the locatin of the first enemy found in the enemies
	# array post-filter
	player.get_parent().add_child(sword_instance);
	
	if (enemies.is_empty()):
		pass
	else:
		sword_instance.global_position = enemies[0].global_position;
		
		sword_instance.global_position += \
			Vector2.RIGHT.rotated(randf_range(0, TAU)) * 4;
			
		var enemy_direction = enemies[0].global_position - \
			sword_instance.global_position

		sword_instance = enemy_direction.angle()
