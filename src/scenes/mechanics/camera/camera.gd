extends Camera2D


func _ready():
	make_current();


func _process(delta):
	acquire_target()
	var target = acquire_target();
	global_position = global_position.lerp(target, 1.0 - exp(-delta * 15));


func acquire_target():
	var player_nodes = get_tree().get_nodes_in_group("player");
	var target_position = Vector2.ZERO;
	
	if(player_nodes.size() > 0):
		var player = player_nodes[0] as Node2D;
		target_position = player.global_position;
	
	return target_position;
